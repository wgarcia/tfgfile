# tfgfile

This is a Python program that I prepared to convert an Excel file produced
by Moodle's database module into a format that a UPF bachelor thesis database
program requires.

It uses the Kivy library to create a GUI.

## Create a Windows executable

A Windows executable can be only created in a Windows installation. Here are
some instructions to create it. 

You need to clone or copy this repository to the Windows machine. Edit the
converter.spec file to substitute \<path to:\>  in the Analysis section with 
the path to the cloned  repository (use "/" or "\\\\" instead of the usual 
Windows "\\" separator in the path).

Install Python and Kivy following the instructions available online:

https://kivy.org/doc/stable/installation/installation-windows.html#installation-windows

In what follows, we assume that "python" is in the Windows path. Install 
PyInstaller. For instance from the Windows command line:

```
python -m pip install --upgrade pyinstaller
```

Install the openpyxl module:

```
python -m pip install openpyxl
```

Create a folder called "Converter", and cd into into it. In that folder
enter the following command:

```
python -m PyInstaller --onefile --windowed --name converter <path to:>convert.py
```

where you have to substitute \<path to:\> with the path to the place where you 
cloned this repository.

Copy the converter.spec file of the repository to the "Converter" folder (it
will overwrite a file with the same name created by the previous command in
the "Converter" folder.) Enter the following command:

```
python -m PyInstaller converter.spec
```

You will find the Windows executable (converter.exe) in the "dist" subfolder
of the "Converter" folder.

