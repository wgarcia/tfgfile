# convert.py
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.popup import Popup
from kivy.uix.spinner import Spinner
from kivy.uix.togglebutton import ToggleButton
from kivy.core.window import Window
import os
import process
from save import save_excel_file

class ExcelProcessor(App):
    def build(self):

        # Set background color
        Window.clearcolor = (21/255, 49/255, 232/255, 1)

        self.title = 'Programa convertidor Base de Dades TFG\nVersió 0.1'

        # Title label
        title_label = Label(text=self.title, font_size='20sp', halign='center', valign='middle')

        # Language selector
        self.language = 'Català'  # Default language
        self.language_label = Label(text='Idioma', size_hint=(1, None), height='50dp', font_size='16sp')

        # ToggleButtons
        self.language_button_cat = ToggleButton(text='Català', group='language', size_hint=(None, None), size=('100dp', '50dp'))
        self.language_button_cat.bind(on_press=self.update_language)
        self.language_button_eng = ToggleButton(text='Anglès', group='language', size_hint=(None, None), size=('100dp', '50dp'))
        self.language_button_eng.bind(on_press=self.update_language)

        # Label for displaying status
        self.status_label = Label(text='')

        # Buttons
        btn_open_excel = Button(text='Obre el fitxer Excel', color=(1, 1, 1, 1), size_hint=(None, None), size=(200, 50))
        btn_open_excel.bind(on_press=self.show_open_file_chooser)
        btn_save_excel = Button(text='Desa el fitxer Excel', color=(1, 1, 1, 1), size_hint=(None, None), size=(200, 50))
        btn_save_excel.bind(on_press=self.save_excel)
        btn_close = Button(text='Tanca', color=(1, 1, 1, 1), size_hint=(None, None), size=(200, 50))
        btn_close.bind(on_press=self.close_app)

        # Set pos_hint to center each widget horizontally
        title_label.pos_hint = {"center_x": 0.5}
        self.language_label.pos_hint = {"center_x": 0.5}
        self.language_button_cat.pos_hint = {"center_x": 0.5}
        self.language_button_eng.pos_hint = {"center_x": 0.5}
        self.status_label.pos_hint = {"center_x": 0.5}
        btn_open_excel.pos_hint = {"center_x": 0.5}
        btn_save_excel.pos_hint = {"center_x": 0.5}
        btn_close.pos_hint = {"center_x": 0.5}

        # Add widgets to the layout
        self.root = BoxLayout(orientation='vertical', padding=10, spacing=10)
        self.root.add_widget(title_label)
        self.root.add_widget(self.language_label)
        self.root.add_widget(self.language_button_cat)
        self.root.add_widget(self.language_button_eng)
        self.root.add_widget(self.status_label)
        self.root.add_widget(btn_open_excel)
        self.root.add_widget(btn_save_excel)
        self.root.add_widget(btn_close)

        return self.root

    def update_language(self, instance):
        self.language = instance.text

    def show_open_file_chooser(self, instance):
        # Create a popup window for opening Excel file
        open_popup = Popup(title='Obre el fitxer Excel', size_hint=(0.8, 0.6))
        open_popup.content = BoxLayout(orientation='vertical', padding=10, spacing=10)

        # File chooser
        open_file_chooser = FileChooserListView()
        initial_dir = os.path.expanduser("~")  # Example initial directory
        open_file_chooser.path = initial_dir
        open_popup.content.add_widget(open_file_chooser)

        # "Convert" button
        btn_convert = Button(text='Convert', size_hint=(None, None), size=(200, 50))
        btn_convert.bind(on_press=lambda instance: self.convert_and_close(open_file_chooser.selection[0], open_popup))
        open_popup.content.add_widget(btn_convert)

        # Open the popup window
        open_popup.open()

    def convert_and_close(self, file_path, popup_instance):
        if file_path:
            new_workbook = process.process_excel_file(file_path, language=self.language)
            if new_workbook:
                save_dir = os.path.dirname(file_path)
                file_name = os.path.basename(file_path)
                file_name_mod = file_name.replace('.xlsx', '_mod.xlsx')
                save_file_path = os.path.join(save_dir, file_name_mod)
                save_excel_file(new_workbook, save_file_path)
                self.status_label.text = '''El fitxer s'ha processat correctament.'''
        else:
            self.status_label.text = 'Hi ha hagut un error processant el fitxer.'
        popup_instance.dismiss()

    def save_excel(self, instance):
        selected_file = self.open_file_chooser.selection
        if selected_file:
            new_workbook = process.process_excel_file(selected_file[0], language=self.language)
            if new_workbook:
                save_excel_file(new_workbook, os.path.expanduser("~"))

    def close_app(self, instance):
        App.get_running_app().stop()

if __name__ == '__main__':
    Window.size = (800, 600)  # Set initial window size
    ExcelProcessor().run()
