# save.py
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout  # Add this import statement
from kivy.core.window import Window
import os

def save_excel_file(workbook, default_dir):
    save_popup = Popup(title='Desa el fitxer Excel', size_hint=(0.8, 0.6))
    save_popup.content = BoxLayout(orientation='vertical', padding=10, spacing=10)

    label = Label(text='Entreu el nom del fitxer a desar:')
    text_input = TextInput(text=default_dir)
    btn_save = Button(text='Desa')
    btn_cancel = Button(text='Cancel·la')

    def save_file(instance):
        file_name = text_input.text
        if file_name:
            save_file_path = os.path.join(default_dir, file_name)
            if os.path.exists(save_file_path):
                # If the file already exists, ask for confirmation
                confirm_popup = Popup(title='Confirmació', size_hint=(0.8, 0.4))
                confirm_popup.content = BoxLayout(orientation='vertical', padding=10, spacing=10)
                confirm_label = Label(text=f'El fitxer "{file_name}" ja existeix. Desitgeu sobreescriure\'l?')
                confirm_btn_yes = Button(text='Sí', size_hint=(None, None), size=(100, 50))
                confirm_btn_no = Button(text='No', size_hint=(None, None), size=(100, 50))

                def overwrite_file(_):
                    try:
                        workbook.save(save_file_path)
                        print(f'El fitxer Excel "{file_name}" s\'ha sobrescrit correctament.')
                        save_popup.dismiss()
                        confirm_popup.dismiss()
                    except Exception as e:
                        print("Hi ha hagut un error processant el fitxer:", e)

                def cancel_overwrite(_):
                    confirm_popup.dismiss()

                confirm_btn_yes.bind(on_press=overwrite_file)
                confirm_btn_no.bind(on_press=cancel_overwrite)

                confirm_popup.content.add_widget(confirm_label)
                confirm_popup.content.add_widget(confirm_btn_yes)
                confirm_popup.content.add_widget(confirm_btn_no)

                confirm_popup.open()
            else:
                try:
                    workbook.save(save_file_path)
                    print(f'El fitxer Excel "{file_name}" s\'ha desat correctament com a: {save_file_path}')
                    save_popup.dismiss()
                except Exception as e:
                    print("Hi ha hagut un error processant el fitxer:", e)
        else:
            print("El nom del fitxer no pot estar buit.")

    def cancel(instance):
        save_popup.dismiss()

    btn_save.bind(on_press=save_file)
    btn_cancel.bind(on_press=cancel)

    save_popup.content.add_widget(label)
    save_popup.content.add_widget(text_input)
    save_popup.content.add_widget(btn_save)
    save_popup.content.add_widget(btn_cancel)

    save_popup.open()
