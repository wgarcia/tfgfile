# process.py
from openpyxl import load_workbook, Workbook
import os
import save

def process_excel_file(input_file, language='Català'):
    try:
        workbook = load_workbook(filename=input_file)
        sheet = workbook.active

        for i in range(1, 5):
            if i == 1:
                header = f"Autor/a {i}" if language == 'Català' else f"Author {i}"
            else:
                header = f"Autor/a {i} (si s'escau)" if language == 'Català' else f"Author {i} (if needed)"
            sheet.cell(row=1, column=27+i, value=header)

            for row_idx in range(2, sheet.max_row + 1):
                first_name = sheet.cell(row=row_idx, column=(i - 1) * 3 + 16).value
                middle_name = sheet.cell(row=row_idx, column=(i - 1) * 3 + 17).value
                last_name = sheet.cell(row=row_idx, column=(i - 1) * 3 + 18).value

                if first_name and middle_name:
                    if last_name:
                        full_name = f"{middle_name} {last_name}, {first_name}"
                    else:
                        full_name = f"{middle_name}, {first_name}"
                    sheet.cell(row=row_idx, column=27+i, value=full_name)
            # Create a new workbook
            new_workbook = Workbook()
            new_sheet = new_workbook.active
            # Copy data from columns 4, 5, and 6 of the existing workbook to columns 1, 2, and 3 of the new workbook
            for row_idx in range(1, sheet.max_row + 1):
                for col_idx in range(1, 4):
                    cell_value = sheet.cell(row=row_idx, column=col_idx).value
                    new_sheet.cell(row=row_idx, column=col_idx, value=cell_value)
                for col_idx in range(4, 9):
                    cell_value = sheet.cell(row=row_idx, column=col_idx).value
                    new_sheet.cell(row=row_idx, column=col_idx+11, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=28).value
                new_sheet.cell(row=row_idx, column=4, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=29).value
                new_sheet.cell(row=row_idx, column=6, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=30).value
                new_sheet.cell(row=row_idx, column=8, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=31).value
                new_sheet.cell(row=row_idx, column=10, value=cell_value)

            if language == 'Català':
                new_sheet.cell(row=1, column=5, value="Grau autor/a 1")
                new_sheet.cell(row=1, column=7, value="Grau autor/a 2 (si s'escau)")
                new_sheet.cell(row=1, column=9, value="Grau autor/a 3 (si s'escau)")
                new_sheet.cell(row=1, column=11, value="Grau autor/a 4 (si s'escau)")
                new_sheet.cell(row=1, column=12, value="Tutor/a de la UPF")
                new_sheet.cell(row=1, column=13, value="Tutor/a extern 1 (si s'escau)")
                new_sheet.cell(row=1, column=14, value="Tutor/a extern 2 (si s'escau)")
                new_sheet.cell(row=1, column=20, value="Etiquetes")
            else:  # English
                new_sheet.cell(row=1, column=5, value="Degree of author 1")
                new_sheet.cell(row=1, column=7, value="Degree of author 2 (if needed")
                new_sheet.cell(row=1, column=9, value="Degree of author 3 (if needed")
                new_sheet.cell(row=1, column=11, value="Degree of author 4 (if needed")
                new_sheet.cell(row=1, column=12, value="UPF tutor")
                new_sheet.cell(row=1, column=13, value="External tutor 1 (if needed)")
                new_sheet.cell(row=1, column=14, value="External tutor 2 (if needed)")
                new_sheet.cell(row=1, column=20, value="Tags")

            for row_idx in range(2, sheet.max_row + 1):
                cell_value = sheet.cell(row=row_idx, column=9).value
                new_sheet.cell(row=row_idx, column=5, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=10).value
                new_sheet.cell(row=row_idx, column=7, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=11).value
                new_sheet.cell(row=row_idx, column=9, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=13).value
                new_sheet.cell(row=row_idx, column=12, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=14).value
                new_sheet.cell(row=row_idx, column=13, value=cell_value)
                cell_value = sheet.cell(row=row_idx, column=15).value
                new_sheet.cell(row=row_idx, column=14, value=cell_value)
        return new_workbook
    except Exception as e:
        print('''S'ha produït un error en processar el fitxer Excel:''', e)
        return None
